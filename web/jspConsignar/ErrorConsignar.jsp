<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Error al Consignar</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" type="text/css" href="./css/main.css">
        <!--===============================================================================================--></head>
    <body>

        <div class="menu">

            <div class="contenedorMenu" style="background-image: url('./images/bg-01.jpg');">

                <div class="Menu">
                    <span class="Titulo">Sistema Banco
                    </span>
                    <span class="Titulo">Operacion Fallida
                    </span>
                    <span class="Titulo">No se pudo Consignar el dinero
                    </span>
                    <form class="Cuadro" action="./index.jsp">
                        <input type="submit" class="Boton" value="Volver" name="registrar" class="register-button" />
                    </form>
                </div>
            </div>
        </div>
    </body>

</html>