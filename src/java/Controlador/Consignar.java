/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Dto.Cuenta;
import Dto.Movimiento;
import Dto.TipoMovimiento;
import Negocio.Banco;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author USER
 */
public class Consignar extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        Banco banco = new Banco();
        if (request.getSession().getAttribute("banco") != null) {
            banco = (Banco) request.getSession().getAttribute("banco");
        }

        int tipo = Integer.parseInt(request.getParameter("tipo"));
        int cuenta = Integer.parseInt(request.getParameter("cuenta"));
        int cantidad = Integer.parseInt(request.getParameter("cantidad"));

        LocalDate f = LocalDate.now();
        Date fecha = new Date(f.getYear(), f.getMonth().getValue(), f.getDayOfMonth());

        Cuenta cu = new Cuenta(cuenta);

        if (cu != null) {
            Movimiento m = new Movimiento();

            m.setFecha(fecha);
            m.setNroCuenta(cu);
            m.setValor(cantidad);
            m.setIdTipoMovimiento(new TipoMovimiento(1));

            if (banco.Consignar(m)) {
                request.getSession().setAttribute("banco", banco);
                request.getRequestDispatcher("./jspConsignar/ConsignarExito.jsp").forward(request, response);
            }
        } else {
            request.getRequestDispatcher("./jspConsignar/ErrorConsignar.jsp").forward(request, response);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
