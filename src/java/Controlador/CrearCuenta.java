/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Dto.Cliente;
import Dto.Cuenta;
import Dto.Tipo;
import Negocio.Banco;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JOptionPane;

/**
 *
 * @author USER
 */
public class CrearCuenta extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        Banco banco = new Banco();
        
        if(request.getSession().getAttribute("banco")!=null){
            banco=(Banco)request.getSession().getAttribute("banco");
        }
                
        
        int tipo = Integer.parseInt( request.getParameter("tipo"));
        int cedula = Integer.parseInt(request.getParameter("cedula"));
        LocalDate f= LocalDate.now();
        Date fecha= new Date(f.getYear(), f.getMonth().getValue(), f.getDayOfMonth());
        int numeroCuenta=(int)(Math.random()*100000000);
        
        Cuenta cu = new Cuenta();
        Tipo t= new Tipo(tipo);
        
        cu.setCedula(new Cliente(cedula));
        cu.setFechacreacion(fecha);
        cu.setNroCuenta(numeroCuenta);
        cu.setSaldo(0);
        cu.setTipo(t);
        
        if(banco.agregarCuenta(cu)){
        request.getSession().setAttribute("banco", banco);
           request.getRequestDispatcher("./jspCuenta/CuentaCreada.jsp").forward(request, response);
        }else{
           request.getRequestDispatcher("./jspCuenta/ErrorCuenta.jsp").forward(request, response); 
        } 
        
        
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
