/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Dao.ClienteJpaController;
import Dao.Conexion;
import Dto.Cliente;
import Dto.Cuenta;
import Negocio.Banco;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author USER
 */
public class Buscar extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        Conexion con= Conexion.getConexion();
        ClienteJpaController cliente = new ClienteJpaController(con.getBd());
        List<Cliente> clientes=cliente.findClienteEntities();
        
        
        
        Banco banco = new Banco();
        if(request.getSession().getAttribute("banco")!=null){
            banco=(Banco)request.getSession().getAttribute("banco");
        }
        
        
        int cedula = Integer.parseInt( request.getParameter("cedula"));
        Cliente c=new Cliente();
        c.setCedula(cedula);
        
        
        if(clientes.contains(c)){
            
           //response.sendRedirect("./jspEditar/editar.html");
           request.getSession().setAttribute("banco", banco);
           request.getRequestDispatcher("./jspEditar/Encontro.jsp").forward(request, response);
           request.getSession().setAttribute("cedula", cedula);
          
        } 
        
        
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        int cedula = Integer.parseInt( request.getParameter("cedula"));
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
