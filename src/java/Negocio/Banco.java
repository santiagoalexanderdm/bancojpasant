/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Dao.ClienteJpaController;
import Dao.Conexion;
import Dao.CuentaJpaController;
import Dao.MovimientoJpaController;
import Dto.Cliente;
import Dto.Cuenta;
import Dto.Movimiento;
import Dto.Tipo;
import Dto.TipoMovimiento;
import java.time.LocalDate;
import java.util.ArrayList;
import static java.util.Collections.list;
import java.util.Date;
import java.util.List;


public class Banco {
    
    public List getClientes(){
       
        Conexion con= Conexion.getConexion();
        
        ClienteJpaController cl= new ClienteJpaController(con.getBd());
        
        return cl.findClienteEntities();
        
    } 
    
    public List getCuentas(){
        Conexion con= Conexion.getConexion();
        
        CuentaJpaController cl= new CuentaJpaController(con.getBd());
        
        return cl.findCuentaEntities();
    }
    
    public List getOperaciones(){
         Conexion con= Conexion.getConexion();
         
         MovimientoJpaController mo = new MovimientoJpaController(con.getBd());
         
         return mo.findMovimientoEntities();
    }
    
    public List getOperacionesPorCuenta(long nro){
        
        Conexion con=Conexion.getConexion();
        
        MovimientoJpaController m = new MovimientoJpaController(con.getBd());
        
        List<Movimiento> mov= new ArrayList();
         List<Movimiento> movs = m.findMovimientoEntities();
        for(Movimiento mo: movs){
            
            if(mo.getNroCuenta().getNroCuenta()== nro){
                
                mov.add(mo);
            }
            
        }
        
        return mov;
    }
    
    public boolean Consignar( Movimiento m){
        
        Conexion con = Conexion.getConexion();
        
        CuentaJpaController mov= new CuentaJpaController(con.getBd());
        MovimientoJpaController movs = new MovimientoJpaController(con.getBd());
        Integer in=(int)(long) m.getNroCuenta().getNroCuenta();
        Cuenta c = mov.findCuenta(in);
        
        c.setSaldo(c.getSaldo()+m.getValor());
        try{
        mov.edit(c);
        movs.create(m);
        return true;
        }catch(Exception e){
            System.err.print(e);
            return false;
        }
    }
    
    public boolean retirar(Movimiento m){
        
        Conexion con = Conexion.getConexion();
        CuentaJpaController mov= new CuentaJpaController(con.getBd());
        MovimientoJpaController movs = new MovimientoJpaController(con.getBd());
        Integer in=(int)(long) m.getNroCuenta().getNroCuenta();
        Cuenta c = mov.findCuenta(in);
        
        if(c.getSaldo()>= m.getValor()){
            c.setSaldo(c.getSaldo()-m.getValor());
            try{
            mov.edit(c);
            movs.create(m);
            return true;
            }catch(Exception e){
                System.err.print(e);
                return false;
            }
            
        }
       
        return false;
    }
    
    public boolean transferir(Movimiento m, long cuentaDestino){
        Conexion con = Conexion.getConexion();
        CuentaJpaController cu= new CuentaJpaController(con.getBd());
        
        Cuenta c1=cu.findCuenta(m.getNroCuenta().getNroCuenta());
        Cuenta c2=cu.findCuenta((int)(long)cuentaDestino);
        
       boolean v= this.retirar(m);
        m.setNroCuenta(c2);
        if (v){
        return this.Consignar(m);
        }
        return false;
    }
    
    
    
    public boolean agregarCliente(Cliente c){
        
        Conexion con = Conexion.getConexion();
        
        ClienteJpaController cli= new ClienteJpaController(con.getBd());
        try{
        cli.create(c);
        return true;
        }catch(Exception e){
            System.err.print(e);
            return false;
        }
        
    }
    
    public boolean cambios(Cliente c){
        Conexion con = Conexion.getConexion();
        ClienteJpaController cli = new ClienteJpaController(con.getBd());
        
        try{
            cli.edit(c);
            return true;
        }catch(Exception e){
            System.err.print(e);
            return false;
        }
    }
    
    public boolean agregarCuenta(Cuenta cu){
        Conexion con = Conexion.getConexion();
        
        CuentaJpaController cue= new CuentaJpaController(con.getBd());
        ClienteJpaController cli= new ClienteJpaController(con.getBd());
        if(cli.findCliente(cu.getCedula().getCedula())!=null){
        try{
            cue.create(cu);
            return true;
            
        }catch(Exception e){
            System.err.print(e);
            return false;
        }}
        System.out.print("no se encontro el cliente");
        return false;
    }
    
//    public boolean agregarCuenta2(){
//        
//    }
    
    public static void main(String [] args){
      Banco b= new Banco();
      //b.agregarCliente(new Cliente(10909283, "pedro", new Date(23, 56, 67), "cll 45 - 67", 30239239, "fegc@live.com"));
//      Cuenta c = new Cuenta();
//      c.setCedula(new Cliente(10909283));
//      c.setFechacreacion(new Date(12, 30, 40));
//      c.setNroCuenta(101393934);
//      c.setTipo(new Tipo(2));
//      c.setSaldo(0);
//      b.agregarCuenta(c);
        Movimiento m= new Movimiento();
//       //LocalDate f= LocalDate.now();
//        m.setFecha(new Date(f.getYear(), f.getDayOfYear(), f.getDayOfMonth()));
//        m.setIdTipoMovimiento(new TipoMovimiento(2));
//        m.setNroCuenta(new Cuenta(101393934));
//        m.setValor(200);
        LocalDate f= LocalDate.now();
       System.out.print(f.getYear() + " "+ f.getMonthValue()+" " + f.getDayOfMonth());
       Date fecha= new Date(f.getYear(),f.getMonthValue(), f.getDayOfMonth());
       System.out.print(fecha.toString());
       
//        b.retirar(m);
//        System.out.print(b.getCuentas().toString());
    }
    
}
